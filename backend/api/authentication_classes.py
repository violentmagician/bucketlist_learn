from rest_framework.authentication import TokenAuthentication


class ApiCustomTokenAuthentication(TokenAuthentication):
    """Customize TokenAuthentication to use Bearer instead of Authorisation"""

    keyword = "Bearer"

