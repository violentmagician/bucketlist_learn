"""Custom permissions"""
from rest_framework.permissions import BasePermission
from .models import Bucketlist
import logging

logger = logging.getLogger("API.permissions")


class IsOwner(BasePermission):
    """Custom permission class to allow only owners to edit"""

    def has_object_permission(self, request, view, obj):
        """Return true if permission is granted to the bucketlist owner."""
        if isinstance(obj, Bucketlist):
            return obj.owner == request.user
        logger.error(obj)
        return obj.owner == request.user
