from rest_framework import generics, permissions
from .serializers import BucketlistSerializer
from .models import Bucketlist
from .permissions import IsOwner


class CreateView(generics.ListCreateAPIView):
    """This class defines the create behavior of our rest api."""

    serializer_class = BucketlistSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner)

    def perform_create(self, serializer):
        """Save the post data when creating a new bucketlist."""
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        """View must only return buckelists for the current user"""
        user = self.request.user
        return Bucketlist.objects.filter(owner=user)


class DetailsView(generics.RetrieveUpdateDestroyAPIView):
    """Individual buckelist items"""

    queryset = Bucketlist.objects.all()
    serializer_class = BucketlistSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner)
